#Create some bogus information to use in our post db
20.times do |i|
  post = Post.create(title: "Post #{i}", content: "Some awesome content")
  5.times { |j| post.comments.create(content: "Comment n°#{j}") }
end